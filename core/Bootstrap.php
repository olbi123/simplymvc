<?php

/** 
 * Check if autoload file exists
 * If autoload file exists then load it
 * If autoload file doesnt exist then throw error
 */

if(file_exists(VENDOR . 'autoload.php')) 
{
    require VENDOR . 'autoload.php';
} 
else 
{
    throw new Exception('autoload.php file was not found in ' . VENDOR);
}

/** 
 * Check if config file exists
 * If config file exists then load it
 * If config file doesnt exist then throw error
*/ 

if(is_readable(APP . 'config/config.php')) 
{
    require APP . 'config/config.php';
} 
else 
{
    throw new Exception('config.php file was not found in' . APP . 'config');
}

$router = new AppWorld\FrostHeart\Router();

$view = new AppWorld\FrostHeart\View(
	new AppWorld\FrostHeart\ViewLoader(APP . 'views' . DIRECTORY_SEPARATOR),
        new AppWorld\FrostHeart\Templater());