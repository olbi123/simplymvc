<?php
namespace AppWorld\FrostHeart;

class Router{

    private $routes = [];
    private $notFound;

    public function __construct(){
        $this->notFound = function($url){
            echo "404 - $url was not found!";
        };
    }

    public function add($url, $action){
        $this->routes[$url] = $action;
    }

    public function setNotFound($action){
        $this->notFound = $action;
    }

    public function dispatch(){
        foreach ($this->routes as $url => $action) {

                if($_SERVER['REQUEST_URI'] == $url) {
                    
                    $actionsArray = explode("@", $action);
                    $method = $actionsArray[1];
                    $controllerNS = 'AppWorld\\Controls\\'.$actionsArray[0];
                    
                    return (new $controllerNS)->$method();
                    
                }
                
        }
        call_user_func_array($this->notFound,[$_SERVER['REQUEST_URI']]);
    }
}

