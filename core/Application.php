<?php
namespace AppWorld\FrostHeart;
use AppWorld\Controls as Controls;

class Application {
    
    // $var::Mixed - Instance of the controller
    public $controller;
    // $var::Array - Parameters passed to the URL
    public $parameters = array();  
    // $var::String - Current controller name
    public $controller_name;
    // $var::String - Current method name
    public $method_name;
    
    public function __construct() {
        //Prepare URL and set URL parameters
        $this->prepareURL();
        //Check if controller and/or method are not empty
        $this->checkControllerAndMethod();
    }
    
    private function prepareURL() {
        
        //GET URL and split it to URL Segments
        $params = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
        $params = trim($params, '/');
        $params = filter_var($params, FILTER_SANITIZE_URL);
        $params = explode('/', $params);

        //Set controller name to first URL Segment within GET['action']
        $this->controller_name = $params[0];
        //Set method name to first URL Segment within GET['action']
        $this->method_name = $params[1];     

        unset($params[0], $params[1]);

        //Store array of parameters(URL Segments) to parameters array
        $this->parameters = array_values($params);
        
        echo '<pre>';
        var_dump($this->parameters);
        echo '</pre>';
    }
    
    /**
     * 
     * This method checks if the controller and/or method was provided in the URL 
     * If the controller and/or method was not provided then defaults are loaded from config file.
     * Also renames the Controller name so it's landingController not just landing.
     * 
     */
    
    private function checkControllerAndMethod() {
        
        //Check whether the controller_name is set if not load default controller
        if(!$this->controller_name) 
        {
            $this->controller_name = DEFAULT_CONTROLLER;
        }
        
        //Check whether the method_name is set if not load default method
        if(!$this->method_name || strlen($this->method_name) === 0) 
        {
            $this->method_name = DEFAULT_METHOD;
        }
        
        //Rename the controller_name so that it contains "Controller" word after the name
        $this->controller_name = $this->controller_name . 'Controller';
        
        /**
        echo 'Controller: ' . $this->controller_name;
        echo '<br />';
        echo 'Method: ' . $this->method_name;
        **/
    }
}
