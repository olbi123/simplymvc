<?php
namespace AppWorld\FrostHeart;

class ViewLoader {

    public function __construct($viewDir) {
        
        $this->viewDir = $viewDir;
        
    }
    
    public function loadView($viewFileName) {
        
        $viewFilePath = APP . 'view' . DIRECTORY_SEPARATOR . $viewFileName . '.php';
        
        if(file_exists($viewFilePath)) {
            file_get_contents($viewFilePath);
        }
        else 
        {
            throw new Exception($viewFileName . '.php file was not found in ' . APP . 'view' . DIRECTORY_SEPARATOR);
        }
    }
    
}

