<?php
namespace AppWorld\FrostHeart;

class Templater {
       
    public function replaceVariables(&$view, $data){
        

        $view = preg_replace_callback('/(\\{)(\\{)((?:[a-zA-Z]*))(\\})(\\})/',
        function($match) use($data){
            return $data[$match[3]];
        }, $view);

        
        
    }

    public function parse($view, $data){

        $view = $this->replaceVariables($view, $data);
        
        return $view;
    }
}