<?php
namespace AppWorld\FrostHeart;
use AppWorld\FrostHeart\View;
use AppWorld\FrostHeart\ViewLoader;
use AppWorld\FrostHeart\Templater;

abstract class baseController {
    
    public $view;
    
    public function __construct() {

        $this->view = new View(
            new ViewLoader(APP . '/view/'),
            new Templater()
        );
        
    }
    
}