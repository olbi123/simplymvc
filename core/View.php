<?php
namespace AppWorld\FrostHeart;

class View {

    public function __construct($viewLoader, $viewEngine) {
        
        $this->viewLoader = $viewLoader;
        $this->viewEngine = $viewEngine;
        
    }
    
    public function render($viewName, $data = []) {
        
        echo $this->viewEngine->parse($this->viewLoader->loadView($viewName), $data);
        
    }
    
}

