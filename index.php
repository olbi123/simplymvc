<?php

// Define root directory of index.php, default is public_html
define('ROOT', __DIR__);

// Define app directory of index.php, default is public_html/application
define('APP', ROOT . DIRECTORY_SEPARATOR . "application" . DIRECTORY_SEPARATOR);

// Define assets directory, default is public_html/assets
define('ASSET', ROOT . DIRECTORY_SEPARATOR . "asets" . DIRECTORY_SEPARATOR);

// Define vendor directory, default is public_html/vendor
define('VENDOR', ROOT . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR);

require('core/Bootstrap.php');
require('application/routes.php');

$router->dispatch();


switch(ENVIRONMENT) 
{
    case 'development':
	error_reporting(-1);
	ini_set('display_errors', 1);
        break;
    case 'live':
        error_reporting(0);
        ini_set('display_errors', 0);
        break;
    default:
        throw new Exception('Application ENVIRONMENT is set incorrectly please check comments for details');       
}

/**
foreach(get_required_files() as $value) {
    echo $value.'<br />';
}

echo '<br />';

foreach(get_declared_classes() as $value) {
    echo $value.'<br />';
}
**/