<?php
namespace AppWorld\Confyy;

/**
 * APPLICATION SETTINGS
 */

//Define application environment
define("ENVIRONMENT", "development");
//Define base url
define("BASE_URL", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);          
//Define views directory
define("VIEW_DIR", APP . "view/");

/**
 * DATABASE SETTINGS
 */

//Define DB Server
define("DB_SERVER", "localhost");
//Define DB User 
define("DB_USER", "root");
//Define DB Password
define("DB_PASSWORD", "");
//Define DB Name
define("DB_NAME", "test");
